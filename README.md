**Sudoku Solver CLI is completed!**

**Some important stuff:**
- File sudoku_solver.cpp includes main function
- U can select prefered "Logging" type,  uncommenting CONSOLE_LOGGING and/or FILE_LOGGING macro in sudoku.h file (**FILE_LOGGING in LOGS.txt is selected by default**)
- U need to define CONSOLE_CLEAR macro in sudoku.h 
**by default: cls**
(if you are using Linux/BSD comment current macro & uncomment line below)
- Valid VS Solution(.sln) file!
- U can find some examples in Unit-Test's directory
- Input data example:
> 0 0 6 4 0 0 1 5 0\
> 0 0 0 0 2 0 0 6 3\
> 2 0 5 0 0 3 4 0 0\
> 0 3 0 1 0 9 2 0 0\
> 0 0 0 0 7 0 3 9 6\
> 1 0 0 0 0 0 0 0 8\
> 5 0 3 2 6 0 0 0 0

Enjoy >.<

**Features:**
- Solves any sudoku! (if it's possible)
- Unit-tests included
- Two types of Logs
- Minimalistic CLI  ^_^
