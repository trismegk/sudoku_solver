#include "pch.h"
#include "CppUnitTest.h"
#include <vector>
#define N 9
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest {
	TEST_CLASS(UnitTest1) {
private:
	std::vector<std::vector<int>> sudoku_board;
	std::vector<std::vector<int>> sudoku_board_res;
	int i, j;
public:
	bool sudoku_solved()
	{
		if (field_solved())
			return true;
		int r, c, key_value_it;
		for (r = 0; r < 9; r++)
		{
			for (c = 0; c < 9; c++)
			{
				if (sudoku_board[r][c] == 0)
				{
					for (key_value_it = 1; key_value_it < 10; key_value_it++)
					{
						if (field_val_valid(r, c, key_value_it))
						{
							sudoku_board[r][c] = key_value_it;
							if (sudoku_solved())
								return true;
							sudoku_board[r][c] = 0;
						}
					}
					if (sudoku_board[r][c] == 0)
						return false;
				}
			}
		}
		return false;
	};
	bool field_val_valid(int r_arg, int c_arg, int key_value)
	{
		int r, c;
		for (r = 0; r < 9; r++)
		{
			if (r == r_arg)
				continue;
			if (sudoku_board[r][c_arg] == key_value)
			{
				return false;
			}
		}
		for (c = 0; c < 9; c++)
		{
			if (c == c_arg)
				continue;
			if (sudoku_board[r_arg][c] == key_value)
			{
				return false;
			}
		}
		if (!square_3x3_valid(r_arg, c_arg, key_value))
		{
			return false;
		}
		return true;
	};
	bool square_3x3_valid(int r_arg, int c_arg, int key_value)
	{
		int r, c;
		int s_start[3] = { 0, 3, 6 };
		int s_end[3] = { 3, 6, 9 };
		int s_r = int(r_arg / 3);
		int s_c = int(c_arg / 3);
		for (r = s_start[s_r]; r < s_end[s_r]; r++)
		{
			for (c = s_start[s_c]; c < s_end[s_c]; c++)
			{
				if (r == r_arg && c == c_arg)
					continue;

				if (sudoku_board[r][c] == key_value)
					return false;
			}
		}
		return true;
	};
	bool field_solved()
	{
		int flag;

		for (i = 0; i < 9; i++)
		{
			for (j = 0; j < 9; j++)
			{
				if (sudoku_board[i][j] == 0)
					return false;

				flag = field_val_valid(i, j, sudoku_board[i][j]);

				if (flag == 0)
					return false;
			}
		}
		return true;
	};
	TEST_METHOD(TestMethod1)
	{
		UnitTest1 t;
		t.sudoku_board = {
			{6, 0, 0, 0, 0, 5, 0, 0, 4},
			{7 ,0 ,4 ,6 ,2 ,9 ,1 ,0 ,5},
			{0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0},
			{4 ,7 ,0 ,0 ,6 ,0 ,0 ,3 ,0},
			{0 ,8 ,0 ,3 ,0 ,2 ,0 ,5 ,0},
			{0 ,9 ,0 ,0 ,5 ,0 ,0 ,7 ,1},
			{0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0},
			{3 ,0 ,1 ,8 ,9 ,6 ,2 ,0 ,7},
			{9 ,0 ,0 ,5 ,0 ,0 ,0 ,0 ,8}
		};
		t.sudoku_board_res = {
			{6 ,1 ,8 ,7 ,3 ,5 ,9 ,2 ,4},
			{7 ,3 ,4 ,6 ,2 ,9 ,1 ,8 ,5},
			{5 ,2 ,9 ,1 ,8 ,4 ,7 ,6 ,3},
			{4 ,7 ,5 ,9 ,6 ,1 ,8 ,3 ,2},
			{1 ,8 ,6 ,3 ,7 ,2 ,4 ,5 ,9},
			{2 ,9 ,3 ,4 ,5 ,8 ,6 ,7 ,1},
			{8 ,4 ,7 ,2 ,1 ,3 ,5 ,9 ,6},
			{3 ,5 ,1 ,8 ,9 ,6 ,2 ,4 ,7},
			{9 ,6 ,2 ,5 ,4 ,7 ,3 ,1 ,8},
		};
		t.sudoku_solved();
		Assert::IsTrue(t.sudoku_board == t.sudoku_board_res);
	};
	TEST_METHOD(TestMethod2)
	{
		UnitTest1 t;
		t.sudoku_board = {
			{0 ,1 ,0 ,9 ,0 ,0 ,6 ,0 ,5},
			{0 ,2 ,5 ,0 ,6 ,0 ,0 ,7 ,0},
			{8 ,7 ,0 ,0 ,0 ,0 ,9 ,0 ,2},
			{7 ,0 ,2 ,0 ,5 ,0 ,0 ,4 ,3},
			{0 ,0 ,0 ,2 ,0 ,4 ,0 ,0 ,0},
			{4 ,9 ,0 ,0 ,1 ,0 ,5 ,0 ,8},
			{1 ,0 ,7 ,0 ,0 ,0 ,0 ,5 ,6},
			{0 ,4 ,0 ,0 ,8 ,0 ,2 ,1 ,0},
			{2 ,0 ,8 ,0 ,0 ,1 ,0 ,9 ,0},
		};
		t.sudoku_board_res = {
			{3 ,1 ,4 ,9 ,2 ,7 ,6 ,8 ,5},
			{9 ,2 ,5 ,3 ,6 ,8 ,4 ,7 ,1},
			{8 ,7 ,6 ,1 ,4 ,5 ,9 ,3 ,2},
			{7 ,6 ,2 ,8 ,5 ,9 ,1 ,4 ,3},
			{5 ,8 ,1 ,2 ,3 ,4 ,7 ,6 ,9},
			{4 ,9 ,3 ,7 ,1 ,6 ,5 ,2 ,8},
			{1 ,3 ,7 ,4 ,9 ,2 ,8 ,5 ,6},
			{6 ,4 ,9 ,5 ,8 ,3 ,2 ,1 ,7},
			{2 ,5 ,8 ,6 ,7 ,1 ,3 ,9 ,4},
		};
		t.sudoku_solved();
		Assert::IsTrue(t.sudoku_board ==  t.sudoku_board_res);
	};
	TEST_METHOD(TestMethod3)
	{
		UnitTest1 t;
		t.sudoku_board = {
			{4 ,0 ,6 ,2 ,0 ,0 ,0 ,0 ,3},
			{0 ,7 ,9 ,0 ,0 ,0 ,2 ,0 ,1},
			{0 ,0 ,0 ,3 ,0 ,0 ,0 ,0 ,0},
			{6 ,0 ,0 ,0 ,0 ,2 ,5 ,0 ,0},
			{0 ,4 ,0 ,9 ,0 ,3 ,0 ,2 ,0},
			{0 ,0 ,7 ,0 ,1 ,0 ,0 ,0 ,8},
			{0 ,0 ,0 ,0 ,0 ,7 ,0 ,0 ,0},
			{7 ,0 ,4 ,0 ,0 ,0 ,3 ,5 ,0},
			{1 ,0 ,0 ,0 ,0 ,8 ,4 ,0 ,7},

		};
		t.sudoku_board_res = {
			{4 ,5 ,6 ,2 ,7 ,1 ,8 ,9 ,3},
			{3 ,7 ,9 ,8 ,6 ,5 ,2 ,4 ,1},
			{2 ,1 ,8 ,3 ,4 ,9 ,6 ,7 ,5},
			{6 ,9 ,3 ,7 ,8 ,2 ,5 ,1 ,4},
			{8 ,4 ,1 ,9 ,5 ,3 ,7 ,2 ,6},
			{5 ,2 ,7 ,6 ,1 ,4 ,9 ,3 ,8},
			{9 ,6 ,5 ,4 ,3 ,7 ,1 ,8 ,2},
			{7 ,8 ,4 ,1 ,2 ,6 ,3 ,5 ,9},
			{1 ,3 ,2 ,5 ,9 ,8 ,4 ,6 ,7},
		};
		t.sudoku_solved();
		Assert::IsTrue(t.sudoku_board == t.sudoku_board_res);
	}

	TEST_METHOD(TestMethod4)
	{
		UnitTest1 t;
		t.sudoku_board = {
			{0 ,5 ,0 ,9 ,0 ,0 ,1 ,4 ,0},
			{6 ,9 ,0 ,0 ,1 ,0 ,0 ,0 ,0},
			{0 ,0 ,1 ,6 ,0 ,0 ,0 ,9 ,0},
			{2 ,0 ,0 ,0 ,0 ,9 ,0 ,0 ,0},
			{0 ,0 ,7 ,5 ,0 ,4 ,3 ,0 ,0},
			{0 ,0 ,0 ,7 ,0 ,0 ,0 ,0 ,1},
			{0 ,8 ,0 ,0 ,0 ,7 ,5 ,0 ,0},
			{0 ,0 ,0 ,0 ,5 ,0 ,0 ,2 ,8},
			{0 ,1 ,3 ,0 ,0 ,8 ,0 ,7 ,0},
		};
		t.sudoku_board_res = {
			{3 ,5 ,8 ,9 ,7 ,2 ,1 ,4 ,6},
			{6 ,9 ,4 ,8 ,1 ,5 ,7 ,3 ,2},
			{7 ,2 ,1 ,6 ,4 ,3 ,8 ,9 ,5},
			{2 ,3 ,5 ,1 ,8 ,9 ,4 ,6 ,7},
			{1 ,6 ,7 ,5 ,2 ,4 ,3 ,8 ,9},
			{8 ,4 ,9 ,7 ,3 ,6 ,2 ,5 ,1},
			{9 ,8 ,2 ,4 ,6 ,7 ,5 ,1 ,3},
			{4 ,7 ,6 ,3 ,5 ,1 ,9 ,2 ,8},
			{5 ,1 ,3 ,2 ,9 ,8 ,6 ,7 ,4},

		};
		t.sudoku_solved();
		Assert::IsTrue(t.sudoku_board == t.sudoku_board_res);
	}
	TEST_METHOD(TestMethod5)
	{
		UnitTest1 t;
		t.sudoku_board = {
			{0 ,0 ,0 ,0 ,5 ,9 ,0 ,1 ,0},
			{8 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0},
			{0 ,0 ,9 ,1 ,3 ,0 ,0 ,2 ,0},
			{0 ,0 ,0 ,0 ,4 ,0 ,0 ,9 ,7},
			{0 ,0 ,0 ,0 ,0 ,3 ,0 ,4 ,0},
			{0 ,0 ,5 ,9 ,6 ,2 ,0 ,3 ,0},
			{0 ,1 ,3 ,0 ,0 ,7 ,0 ,0 ,5},
			{0 ,4 ,0 ,0 ,0 ,1 ,0 ,0 ,0},
			{0 ,0 ,0 ,0 ,2 ,0 ,0 ,6 ,0},
		};
		t.sudoku_board_res = {
			{3 ,2 ,4 ,8 ,5 ,9 ,7 ,1 ,6},
			{8 ,6 ,1 ,2 ,7 ,4 ,9 ,5 ,3},
			{7 ,5 ,9 ,1 ,3 ,6 ,8 ,2 ,4},
			{1 ,3 ,2 ,5 ,4 ,8 ,6 ,9 ,7},
			{6 ,9 ,8 ,7 ,1 ,3 ,5 ,4 ,2},
			{4 ,7 ,5 ,9 ,6 ,2 ,1 ,3 ,8},
			{2 ,1 ,3 ,6 ,9 ,7 ,4 ,8 ,5},
			{5 ,4 ,6 ,3 ,8 ,1 ,2 ,7 ,9},
			{9 ,8 ,7 ,4 ,2 ,5 ,3 ,6 ,1},

		};
		t.sudoku_solved();
		Assert::IsTrue(t.sudoku_board == t.sudoku_board_res);
	}
	TEST_METHOD(TestMethod6)
	{
		UnitTest1 t;
		t.sudoku_board = {
			{0 ,9 ,0 ,8 ,0 ,0 ,3 ,2 ,0},
			{2 ,4 ,0 ,0 ,1 ,0 ,0 ,0 ,0},
			{0 ,0 ,7 ,9 ,0 ,0 ,0 ,5 ,0},
			{3 ,0 ,0 ,0 ,0 ,4 ,0 ,0 ,0},
			{0 ,0 ,6 ,5 ,0 ,7 ,8 ,0 ,0},
			{0 ,0 ,0 ,6 ,0 ,0 ,0 ,0 ,7},
			{0 ,8 ,0 ,0 ,0 ,3 ,4 ,0 ,0},
			{0 ,0 ,0 ,0 ,9 ,0 ,0 ,7 ,2},
			{0 ,5 ,4 ,0 ,0 ,1 ,0 ,3 ,0},
		};
		t.sudoku_board_res = {
			{6 ,9 ,1 ,8 ,7 ,5 ,3 ,2 ,4},
			{2 ,4 ,5 ,3 ,1 ,6 ,7 ,8 ,9},
			{8 ,3 ,7 ,9 ,4 ,2 ,6 ,5 ,1},
			{3 ,7 ,9 ,1 ,8 ,4 ,2 ,6 ,5},
			{4 ,1 ,6 ,5 ,2 ,7 ,8 ,9 ,3},
			{5 ,2 ,8 ,6 ,3 ,9 ,1 ,4 ,7},
			{9 ,8 ,2 ,7 ,5 ,3 ,4 ,1 ,6},
			{1 ,6 ,3 ,4 ,9 ,8 ,5 ,7 ,2},
			{7 ,5 ,4 ,2 ,6 ,1 ,9 ,3 ,8},

		};
		t.sudoku_solved();
		Assert::IsTrue(t.sudoku_board == t.sudoku_board_res);
	}
	TEST_METHOD(TestMethod7)
	{
		UnitTest1 t;
		t.sudoku_board = {
			{0 ,0 ,7 ,0 ,0 ,4 ,0 ,9 ,2},
			{1 ,0 ,0 ,0 ,0 ,0 ,0 ,5 ,0},
			{0 ,6 ,0 ,5 ,3 ,0 ,0 ,0 ,0},
			{0 ,0 ,9 ,0 ,0 ,0 ,1 ,0 ,5},
			{0 ,0 ,0 ,0 ,2 ,6 ,4 ,0 ,0},
			{3 ,8 ,6 ,0 ,5 ,0 ,0 ,0 ,0},
			{2 ,0 ,8 ,0 ,0 ,7 ,5 ,0 ,0},
			{9 ,1 ,5 ,8 ,0 ,0 ,0 ,0 ,0},
			{0 ,0 ,0 ,2 ,0 ,0 ,8 ,0 ,1},
		};
		t.sudoku_board_res = {
			{5 ,3 ,7 ,1 ,8 ,4 ,6 ,9 ,2},
			{1 ,9 ,4 ,7 ,6 ,2 ,3 ,5 ,8},
			{8 ,6 ,2 ,5 ,3 ,9 ,7 ,1 ,4},
			{4 ,2 ,9 ,3 ,7 ,8 ,1 ,6 ,5},
			{7 ,5 ,1 ,9 ,2 ,6 ,4 ,8 ,3},
			{3 ,8 ,6 ,4 ,5 ,1 ,9 ,2 ,7},
			{2 ,4 ,8 ,6 ,1 ,7 ,5 ,3 ,9},
			{9 ,1 ,5 ,8 ,4 ,3 ,2 ,7 ,6},
			{6 ,7 ,3 ,2 ,9 ,5 ,8 ,4 ,1},

		};
		t.sudoku_solved();
		Assert::IsTrue(t.sudoku_board == t.sudoku_board_res);
	}
	TEST_METHOD(TestMethod8)
	{
		UnitTest1 t;
		t.sudoku_board = {
			{8 ,0 ,0 ,7 ,5 ,0 ,0 ,0 ,4},
			{0 ,0 ,9 ,6 ,0 ,0 ,0 ,5 ,0},
			{0 ,7 ,3 ,2 ,0 ,0 ,6 ,0 ,0},
			{9 ,0 ,1 ,0 ,7 ,0 ,0 ,0 ,0},
			{0 ,0 ,5 ,0 ,0 ,9 ,0 ,2 ,0},
			{0 ,0 ,0 ,8 ,0 ,5 ,9 ,0 ,3},
			{0 ,0 ,0 ,0 ,2 ,7 ,8 ,6 ,0},
			{6 ,1 ,0 ,0 ,4 ,0 ,0 ,0 ,5},
			{7 ,5 ,0 ,0 ,0 ,6 ,0 ,0 ,0},
		};
		t.sudoku_board_res = {
			{8 ,2 ,6 ,7 ,5 ,1 ,3 ,9 ,4},
			{1 ,4 ,9 ,6 ,3 ,8 ,7 ,5 ,2},
			{5 ,7 ,3 ,2 ,9 ,4 ,6 ,1 ,8},
			{9 ,3 ,1 ,4 ,7 ,2 ,5 ,8 ,6},
			{4 ,8 ,5 ,3 ,6 ,9 ,1 ,2 ,7},
			{2 ,6 ,7 ,8 ,1 ,5 ,9 ,4 ,3},
			{3 ,9 ,4 ,5 ,2 ,7 ,8 ,6 ,1},
			{6 ,1 ,8 ,9 ,4 ,3 ,2 ,7 ,5},
			{7 ,5 ,2 ,1 ,8 ,6 ,4 ,3 ,9},
		};
		t.sudoku_solved();
		Assert::IsTrue(t.sudoku_board == t.sudoku_board_res);
	}
	TEST_METHOD(TestMethod9)
	{
		UnitTest1 t;
		t.sudoku_board = {
			{0 ,6 ,9 ,0 ,7 ,0 ,8 ,0 ,4},
			{0 ,0 ,0 ,0 ,6 ,0 ,1 ,0 ,7},
			{5 ,7 ,0 ,8 ,0 ,4 ,0 ,0 ,0},
			{0 ,0 ,0 ,2 ,4 ,8 ,0 ,0 ,0},
			{2 ,0 ,6 ,0 ,0 ,0 ,3 ,0 ,0},
			{4 ,0 ,0 ,0 ,0 ,3 ,0 ,8 ,2},
			{0 ,0 ,5 ,4 ,0 ,0 ,0 ,6 ,0},
			{1 ,4 ,0 ,0 ,3 ,0 ,0 ,0 ,0},
			{0 ,0 ,2 ,1 ,0 ,0 ,4 ,3 ,0},
		};
		t.sudoku_board_res = {
			{3 ,6 ,9 ,5 ,7 ,1 ,8 ,2 ,4},
			{8 ,2 ,4 ,3 ,6 ,9 ,1 ,5 ,7},
			{5 ,7 ,1 ,8 ,2 ,4 ,6 ,9 ,3},
			{9 ,5 ,3 ,2 ,4 ,8 ,7 ,1 ,6},
			{2 ,8 ,6 ,7 ,1 ,5 ,3 ,4 ,9},
			{4 ,1 ,7 ,6 ,9 ,3 ,5 ,8 ,2},
			{7 ,3 ,5 ,4 ,8 ,2 ,9 ,6 ,1},
			{1 ,4 ,8 ,9 ,3 ,6 ,2 ,7 ,5},
			{6 ,9 ,2 ,1 ,5 ,7 ,4 ,3 ,8},
		};
		t.sudoku_solved();
		Assert::IsTrue(t.sudoku_board == t.sudoku_board_res);
	}
	TEST_METHOD(TestMethod10)
	{
		UnitTest1 t;
		t.sudoku_board = {
			{6 ,0 ,1 ,0 ,3 ,0 ,0 ,2 ,0},
			{0 ,0 ,0 ,6 ,0 ,0 ,9 ,5 ,1},
			{7 ,0 ,0 ,0 ,4 ,9 ,0 ,0 ,0},
			{8 ,0 ,6 ,0 ,0 ,0 ,1 ,0 ,0},
			{0 ,0 ,0 ,4 ,0 ,0 ,6 ,0 ,2},
			{4 ,7 ,0 ,9 ,0 ,6 ,8 ,0 ,0},
			{0 ,1 ,0 ,5 ,2 ,0 ,7 ,0 ,3},
			{0 ,0 ,3 ,0 ,0 ,0 ,0 ,9 ,0},
			{2 ,6 ,0 ,0 ,0 ,4 ,0 ,0 ,0},
		};
		t.sudoku_board_res = {
			{6 ,9 ,1 ,8 ,3 ,5 ,4 ,2 ,7},
			{3 ,4 ,8 ,6 ,7 ,2 ,9 ,5 ,1},
			{7 ,2 ,5 ,1 ,4 ,9 ,3 ,8 ,6},
			{8 ,3 ,6 ,2 ,5 ,7 ,1 ,4 ,9},
			{1 ,5 ,9 ,4 ,8 ,3 ,6 ,7 ,2},
			{4 ,7 ,2 ,9 ,1 ,6 ,8 ,3 ,5},
			{9 ,1 ,4 ,5 ,2 ,8 ,7 ,6 ,3},
			{5 ,8 ,3 ,7 ,6 ,1 ,2 ,9 ,4},
			{2 ,6 ,7 ,3 ,9 ,4 ,5 ,1 ,8},
		};
		t.sudoku_solved();
		Assert::IsTrue(t.sudoku_board == t.sudoku_board_res);
	}
	TEST_METHOD(TestMethod11)
	{
		UnitTest1 t;
		t.sudoku_board = {
			{0 ,0 ,0 ,0 ,4 ,7 ,5 ,3 ,0},
			{0 ,3 ,5 ,0 ,0 ,0 ,7 ,0 ,2},
			{0 ,4 ,7 ,0 ,0 ,3 ,1 ,0 ,0},
			{8 ,0 ,3 ,0 ,5 ,0 ,0 ,2 ,0},
			{5 ,0 ,0 ,0 ,7 ,0 ,0 ,6 ,0},
			{0 ,7 ,0 ,6 ,0 ,0 ,0 ,0 ,5},
			{0 ,0 ,0 ,4 ,0 ,9 ,0 ,0 ,8},
			{4 ,1 ,8 ,0 ,0 ,0 ,0 ,0 ,0},
			{3 ,0 ,0 ,1 ,0 ,0 ,2 ,0 ,4},
		};
		t.sudoku_board_res = {
			{6 ,8 ,1 ,2 ,4 ,7 ,5 ,3 ,9},
			{9 ,3 ,5 ,8 ,1 ,6 ,7 ,4 ,2},
			{2 ,4 ,7 ,5 ,9 ,3 ,1 ,8 ,6},
			{8 ,6 ,3 ,9 ,5 ,1 ,4 ,2 ,7},
			{5 ,2 ,9 ,3 ,7 ,4 ,8 ,6 ,1},
			{1 ,7 ,4 ,6 ,2 ,8 ,3 ,9 ,5},
			{7 ,5 ,2 ,4 ,3 ,9 ,6 ,1 ,8},
			{4 ,1 ,8 ,7 ,6 ,2 ,9 ,5 ,3},
			{3 ,9 ,6 ,1 ,8 ,5 ,2 ,7 ,4},
		};
		t.sudoku_solved();
		Assert::IsTrue(t.sudoku_board == t.sudoku_board_res);
	}
	};
};
