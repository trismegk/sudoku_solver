﻿#include"sudoku.h"

#ifdef FILE_LOGGING
	std::ofstream out;
#endif

void log(std::string str)
{
#ifdef CONSOLE_LOGGING
	std::cout << str << "\n";
#endif
#ifdef FILE_LOGGING
	if (out.is_open())
		out << str << "\n";
#endif
}
void log(std::string str, int r)
{
#ifdef CONSOLE_LOGGING
	std::cout << str << r << "\n";
#endif
#ifdef FILE_LOGGING
	if (out.is_open())
		out << str << r << "\n";
#endif
}
void log(std::string str, int r, int c)
{
#ifdef CONSOLE_LOGGING
	std::cout << str << "(" << r << ";" << c << ")" << "\n";
#endif
#ifdef FILE_LOGGING
	if (out.is_open())
		out << str << "(" << r << ";" << c << ")" << "\n";
#endif
}

void log(std::string str, int r, int c, int key_value)
{
#ifdef CONSOLE_LOGGING
	std::cout << str << "(" << r << ";" << c << ") = " << key_value << "\n";
#endif
#ifdef FILE_LOGGING
	if (out.is_open())
		out << str << "(" << r << ";" << c << ") = " << key_value << "\n";
#endif
}

int main()
{
	Sudoku s;
	std::cout << "Sudoku Solver by gitlab.com/trismegk\n\n";

	s.sudoku_input();
	s.sudoku_solved() ? s.sudoku_output() : [] { log("\nIt's impossible to solve current sudoku board!"); } ();

	bool q;
	std::cout << "\nEnter any symbol to close current process:\n";
	std::cin >> q;

	return 0;
}