#include "sudoku.h"

int i, j;

void Sudoku::sudoku_input()
{
	out.open("LOGS.txt");
	std::cout << "Enter " << n << " space separated values (empty field -> 0):" << std::endl;

	for (i = 0; i < n; ++i)
	{
		std::cout << "#" << (i + 1) << ": ";
		for (j = 0; j < n; ++j)
		{
			std::cin >> sudoku_board[i][j];
#ifdef FILE_LOGGING
			if (j == 8)
				out << sudoku_board[i][j] << "\n";
			else
				out << sudoku_board[i][j] << " ";
#endif
		}
	};
};
void Sudoku::sudoku_output()
{
		std::cout << std::endl;
		for (i = 0; i < n; ++i)
			for (j = 0; j < n; ++j)
			{
#ifdef FILE_LOGGING 
				out << sudoku_board[i][j] << (j < n - 1 ? " " : "\n");
#endif
				std::cout << sudoku_board[i][j] << (j < n - 1 ? " " : "\n");
			}
#ifdef FILE_LOGGING
		out.close();
#endif
};
bool Sudoku::sudoku_solved()
{
	system(CONSOLE_CLEAR);
	log("@solver_function #", recursive_count);

	recursive_count++;

	if (field_solved())
		return true;

	int r, c, key_value_it;

	log("[LOG] Lookin for empty fields...");

	for (r = 0; r < 9; r++)
	{
		for (c = 0; c < 9; c++)
		{
			if (sudoku_board[r][c] == 0)
			{
				log("[LOG] Empty field found!", r, c);
				log("[LOG] Lookin for valid key value, unique in a row, column, square...");
				for (key_value_it = 1; key_value_it < 10; key_value_it++)
				{
					if (field_val_valid(r, c, key_value_it))
					{
						log("[LOG] Great! The field", r, c, key_value_it);
						sudoku_board[r][c] = key_value_it;
						if (sudoku_solved())
							return true;
						sudoku_board[r][c] = 0;
					}
				}
				if (sudoku_board[r][c] == 0)
					return false;
			}
		}
	}
	return false;
};
bool Sudoku::field_val_valid(int r_arg, int c_arg, int key_value)
{

	int r, c;

	for (r = 0; r < 9; r++)
	{
		if (r == r_arg)
			continue;

		if (sudoku_board[r][c_arg] == key_value)
		{
			return false;
		}
	}
	for (c = 0; c < 9; c++)
	{

		if (c == c_arg)
			continue;

		if (sudoku_board[r_arg][c] == key_value)
		{
			return false;
		}
	}
	if (!square_3x3_valid(r_arg, c_arg, key_value))
	{
		return false;
	}

	return true;
};
bool Sudoku::square_3x3_valid(int r_arg, int c_arg, int key_value)
{
	int r, c;
	int s_start[3] = { 0, 3, 6 };
	int s_end[3] = { 3, 6, 9 };
	int s_r = int(r_arg / 3);
	int s_c = int(c_arg / 3);

	for (r = s_start[s_r]; r < s_end[s_r]; r++)
	{
		for (c = s_start[s_c]; c < s_end[s_c]; c++)
		{
			if (r == r_arg && c == c_arg)
				continue;

			if (sudoku_board[r][c] == key_value)
				return false;
		}
	}
	return true;
};
bool Sudoku::field_solved()
{

	int flag;

	for (i = 0; i < 9; i++)
	{
		for (j = 0; j < 9; j++)
		{
			if (sudoku_board[i][j] == 0)
				return false;

			flag = field_val_valid(i, j, sudoku_board[i][j]);

			if (flag == 0)
				return false;
		}
	}

	return true;
};