#pragma once
#include<iostream>
//#include<vector>


//#define CONSOLE_LOGGING
#define FILE_LOGGING

//for Windows
#define CONSOLE_CLEAR "cls"
//for Linux/BSD
//#define CONSOLE_CLEAR "clear"

#ifdef FILE_LOGGING
	#include<fstream>;
	extern std::ofstream out;
#endif

extern void log(std::string str);
extern void log(std::string str, int r);
extern void log(std::string str, int r, int c);
extern void log(std::string str, int r, int c, int key_value);

class Sudoku
{
	/*
	   int n;
	   std::cout << "Enter n\n";
	   std::cin >> n;
	   std::vector<std::vector<int>> arr(n, std::vector<int>(n,0));
	*/
private:
	int recursive_count = 0;
	static const int n = 9;
	int sudoku_board[n][n];

public:
	void sudoku_input();
	void sudoku_output();	
	bool sudoku_solved();
	bool field_val_valid(int r_arg, int c_arg, int key_value);
	bool square_3x3_valid(int r_arg, int c_arg, int key_value);
	bool field_solved();
};